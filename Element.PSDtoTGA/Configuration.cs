﻿using System.Configuration;

namespace Element.PSDtoTGA
{
	public class Configuration
	{
		private const string USE_RLE_COMPRESSION = "UseRLECompression";
		private const string PREFERED_NAME = "PreferedName";
		private const string SELECTED_RESOLUTION = "SelectedResolution";
		private const string USE_PREFERED_NAME = "UsePreferedName";
		private readonly System.Configuration.Configuration _config;

		#region Singleton
		private static Configuration _default;
		/// <summary>
		/// Gets the default Configuration.
		/// </summary>
		/// <value>
		/// The default.
		/// </value>
		public static Configuration Default
		{
			get
			{
				if( _default == null )
					_default = new Configuration();

				return _default;
			}
		}
		#endregion

		private Configuration()
		{
			_config = ConfigurationManager.OpenExeConfiguration( System.Reflection.Assembly.GetEntryAssembly().Location );
		}

		/// <summary>
		/// Gets or sets a value indicating whether [use rle compression].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use rle compression]; otherwise, <c>false</c>.
		/// </value>
		public bool UseRLECompression
		{
			get { return bool.Parse( GetAppSetting( USE_RLE_COMPRESSION ) ); }
			set { SetAppSetting( USE_RLE_COMPRESSION, value ); }
		}

		public bool UsePreferedName
		{
			get { return bool.Parse( GetAppSetting(USE_PREFERED_NAME ) ); }
			set { SetAppSetting(USE_PREFERED_NAME, value ); }
		}

		/// <summary>
		/// Gets or sets the name of the prefered.
		/// </summary>
		/// <value>
		/// The name of the prefered.
		/// </value>
		public string PreferedName
		{
			get { return GetAppSetting( PREFERED_NAME ); }
			set { SetAppSetting( PREFERED_NAME, value ); }
		}

		/// <summary>
		/// Gets or sets the selected resolution.
		/// </summary>
		/// <value>
		/// The selected resolution.
		/// </value>
		public int SelectedResolution
		{
			get { return int.Parse( GetAppSetting( SELECTED_RESOLUTION ) ); }
			set { SetAppSetting( SELECTED_RESOLUTION, value ); }
		}



		/// <summary>
		/// Gets the application setting.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private string GetAppSetting( string key )
		{
			return _config.AppSettings.Settings[key].Value;
		}

		/// <summary>
		/// Sets the application setting.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		private void SetAppSetting( string key, object value )
		{
			SetAppSetting( key, value.ToString() );
		}
		private void SetAppSetting( string key, string value )
		{
			
			if( _config.AppSettings.Settings[key] != null )
				_config.AppSettings.Settings.Remove( key );

			_config.AppSettings.Settings.Add( key, value );
			_config.Save( ConfigurationSaveMode.Modified );
		}

	}
}
