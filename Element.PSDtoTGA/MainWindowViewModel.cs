﻿using System;
using Element.MVVM.ViewModels;
using Element.MVVM.Common;
using System.Windows.Input;
using System.IO;
using System.Security.Permissions;
using System.Collections.ObjectModel;
using Element.MVVM;
using System.Windows;
using Ookii.Dialogs.Wpf;

namespace Element.PSDtoTGA
{
	[PermissionSet( SecurityAction.Demand, Name = "FullTrust" )]
	public class MainWindowViewModel : ViewModel
	{
		private FileSystemWatcher _watcher;
		private string _selectedInputFile;
		/// <summary>
		/// Gets or sets the selected input file.
		/// </summary>
		/// <value>
		/// The selected input file.
		/// </value>
		public string SelectedInputFile
		{
			get { return _selectedInputFile; }
			set { SetProperty( ref _selectedInputFile, value, nameof( SelectedInputFile ) ); }
		}

		private string _selectedOutputDir;
		/// <summary>
		/// Gets or sets the selected output dir.
		/// </summary>
		/// <value>
		/// The selected output dir.
		/// </value>
		public string SelectedOutputDir
		{
			get { return _selectedOutputDir; }
			set { SetProperty( ref _selectedOutputDir, value, nameof( SelectedOutputDir ) ); }
		}

		private bool _isListening = false;
		/// <summary>
		/// Gets a value indicating whether this instance is listening.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is listening; otherwise, <c>false</c>.
		/// </value>
		public bool IsListening
		{
			get { return _isListening; }
			private set { SetProperty( ref _isListening, value, nameof( IsListening ) ); }
		}

		/// <summary>
		/// Gets or sets the status collection.
		/// </summary>
		/// <value>
		/// The status collection.
		/// </value>
		public ObservableCollection<string> StatusCollection { get; set; }

		public TGAOptions Options { get; set; }

		/// <summary>
		/// Gets the on file choose command.
		/// </summary>
		/// <value>
		/// The on file choose command.
		/// </value>
		public ICommand OnFileChooseCommand{ get; private set; }

		/// <summary>
		/// Gets the on output dir command.
		/// </summary>
		/// <value>
		/// The on output dir command.
		/// </value>
		public ICommand OnOutputDirCommand { get; private set; }
		/// <summary>
		/// Gets the start file watch command.
		/// </summary>
		/// <value>
		/// The start file watch command.
		/// </value>
		public ICommand StartFileWatchCommand { get; private set; }

		/// <summary>
		/// Gets the stop file wath command.
		/// </summary>
		/// <value>
		/// The stop file wath command.
		/// </value>
		public ICommand StopFileWathCommand { get; private set; }


		public MainWindowViewModel()
		{
			Options = new TGAOptions();
			StatusCollection = new ObservableCollection<string>();

			if( View.IsInDesignMode )
			{
				Log( "New Status 1" );
				Log( "New Status 2" );
				Log( "New Status 3" );
				Log( "New Status 4" );

				return;
			}

			OnFileChooseCommand = new ActionCommand( OpenFileBrowser );
			OnOutputDirCommand = new ActionCommand( OpenDirectoryBrowser, o => !string.IsNullOrEmpty( _selectedInputFile ) );
			StartFileWatchCommand = new ActionCommand( InitializeFileWatcher, o => !string.IsNullOrEmpty( _selectedInputFile  ) && !IsListening );
			StopFileWathCommand = new ActionCommand( StopFileWach, o => IsListening && _watcher != null );
		}

		private void StopFileWach( object obj )
		{
			if( _watcher == null )
				return;

			_watcher.Dispose();
			_watcher = null;

			IsListening = false;
			Log( "Stop listening for changes ..." );
		}

		/// <summary>
		/// Opens the file browser.
		/// </summary>
		/// <param name="o">The object.</param>
		private void OpenFileBrowser( object obj )
		{
			var dialog = new VistaOpenFileDialog();
			dialog.DefaultExt = ".psd";
			dialog.Filter = "Photoshop(.psd ) | *.psd";
			dialog.Multiselect = false;

			bool? result = dialog.ShowDialog();
			if( result == false )
				return;

			SelectedInputFile = dialog.FileName;
			SelectedOutputDir = Path.GetDirectoryName( SelectedInputFile );

		}

		/// <summary>
		/// Opens the directory browser.
		/// </summary>
		/// <param name="obj">The object.</param>
		private void OpenDirectoryBrowser( object obj )
		{
			var dialog = new VistaFolderBrowserDialog();
			var result = dialog.ShowDialog();
			if( result == false )
				return;

			SelectedOutputDir = dialog.SelectedPath;

		}

		/// <summary>
		/// Initializes the file watcher.
		/// </summary>
		/// <param name="obj">The object.</param>
		private void InitializeFileWatcher( object obj )
		{
			_watcher = new FileSystemWatcher();
			_watcher.Path = Path.GetDirectoryName( SelectedInputFile );
			_watcher.Filter = Path.GetFileName( SelectedInputFile );
			_watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

			_watcher.EnableRaisingEvents = true;
			_watcher.Changed += OnFileChanged;
			_watcher.Created += OnFileChanged;
			_watcher.Deleted += OnFileChanged;


			Log( "Listening for changes ..." );
			IsListening = true;
		}

		/// <summary>
		/// Called when [file changed].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="FileSystemEventArgs"/> instance containing the event data.</param>
		private void OnFileChanged( object sender, FileSystemEventArgs e )
		{
			Application.Current.Dispatcher.Invoke( GenerateFile );
		}

		private void GenerateFile()
		{
			var fileName = string.IsNullOrEmpty( Options.PreferedName ) ? Path.GetFileNameWithoutExtension( SelectedInputFile ) : Options.PreferedName;
            var path = Path.Combine( SelectedOutputDir, string.Format( "{0}.tga", fileName ) );

			var app = new Photoshop.Application();
			app.DisplayDialogs = Photoshop.PsDialogModes.psDisplayNoDialogs;
			app.ActiveDocument.SaveAs( path, Options.GetPhotoshopOptions(), false );

			Log( string.Format( "source file changed {0:H:mm:ss}", DateTime.Now ) );
		}

		private void Log( string message )
		{
			StatusCollection.Insert( 0, message );
		}
	}
}
