﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Element.PSDtoTGA.Collections
{
	public class ObservableStack<T> : Stack<T>, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public new void Push( T item )
		{
			base.Push( item );
			RaisePropertyChanged();
        }

		public new T Pop()
		{
			var ret = base.Pop();
			RaisePropertyChanged();
			return ret;
		}

		private void RaisePropertyChanged()
		{
			var handler = PropertyChanged;
			if( handler == null )
				return;
			var e = new PropertyChangedEventArgs( "Count" );
			handler( this, e );
		}
	}
}
