﻿using Element.MVVM;
using System.Collections.ObjectModel;

namespace Element.PSDtoTGA
{
	public class TGAOptions : ObservableObject
	{
		private string _preferedName;
		/// <summary>
		/// Gets or sets the name of the prefered.
		/// </summary>
		/// <value>
		/// The name of the prefered.
		/// </value>
		public string PreferedName
		{
			get { return _preferedName; }
			set
			{
				SetProperty( ref _preferedName, value, nameof( PreferedName ) );
				Configuration.Default.PreferedName = value;
			}
		}

		private bool _useRLECompression;
		/// <summary>
		/// Gets or sets a value indicating whether [use rle compression].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use rle compression]; otherwise, <c>false</c>.
		/// </value>
		public bool UseRLECompression
		{
			get { return _useRLECompression; }
			set
			{
				SetProperty( ref _useRLECompression, value, nameof( UseRLECompression ) );
				Configuration.Default.UseRLECompression = value;
			}
		}
		private bool _usePreferdName;
		public bool UsePreferedName
		{
			get { return _usePreferdName; }
			set
			{
				SetProperty( ref _usePreferdName, value, nameof( UsePreferedName ) );
				Configuration.Default.UsePreferedName = value;
			}
		}

		private readonly ObservableCollection<string> _resolutions = new ObservableCollection<string>() { "16 Bit/Pixel", "24 Bit/Pixel", "32 Bit/Pixel" };
		public ObservableCollection<string> Resolutions
		{
			get { return _resolutions; }
		}

		private int _selectedResolution = 1;
		/// <summary>
		/// Gets or sets the selected resolution.
		/// </summary>
		/// <value>
		/// The selected resolution.
		/// </value>
		public int SelectedResolution
		{
			get { return _selectedResolution; }
			set
			{
				SetProperty( ref _selectedResolution, value, nameof( SelectedResolution ) );
				Configuration.Default.SelectedResolution = value;
			}
		}

		public TGAOptions()
		{
			PreferedName = Configuration.Default.PreferedName;
			SelectedResolution = Configuration.Default.SelectedResolution;
			UseRLECompression = Configuration.Default.UseRLECompression;
			UsePreferedName = Configuration.Default.UsePreferedName;
		}

		/// <summary>
		/// Gets the photoshop options.
		/// </summary>
		/// <returns>Photoshop.TargaSaveOptions</returns>
		public Photoshop.TargaSaveOptions GetPhotoshopOptions()
		{
			var options = new Photoshop.TargaSaveOptions();
			options.RLECompression = UseRLECompression;
			switch( SelectedResolution )
			{
				case 0:
					options.Resolution = Photoshop.PsTargaBitsPerPixels.psTarga16Bits;
					break;
				case 1:
					options.Resolution = Photoshop.PsTargaBitsPerPixels.psTarga24Bits;
					break;
				case 2:
					options.Resolution = Photoshop.PsTargaBitsPerPixels.psTarga32Bits;
					break;
				default:
					options.Resolution = Photoshop.PsTargaBitsPerPixels.psTarga24Bits;
					break;

			}
			return options;
		}

	}
}
