﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Element.PSDtoTGA
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnActivated( EventArgs e )
		{
			AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
			base.OnActivated( e );
		}

		protected override void OnExit( ExitEventArgs e )
		{
			AppDomain.CurrentDomain.UnhandledException -= UnhandledExceptionHandler;
			base.OnExit( e );
		}

		private void UnhandledExceptionHandler( object sender, UnhandledExceptionEventArgs e )
		{
			throw new NotImplementedException();
		}
	}
}
